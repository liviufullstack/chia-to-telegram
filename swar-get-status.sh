#!/bin/bash
# get arguments
while getopts u:a:f: flag
do
    case "${flag}" in
        u) username=${OPTARG};;   
    esac
done

cd /home/$username/chia/Swar-Chia-Plot-Manager
./venv/bin/python manager.py status > /home/$username/chia/chia-to-telegram/status-$username.txt

sensors >> /home/$username/chia/chia-to-telegram/status-$username.txt