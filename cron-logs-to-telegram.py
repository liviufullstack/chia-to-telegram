#!/usr/bin/env python3

# run:
# crontab -e
# add the line to run “At minute 55 past every hour from 6 through 23.”
# 55 6-23 * * * python3 /home/ogica3/chia/chia-to-telegram/cron-logs-to-telegram.py >> /home/ogica3/chia/chia-to-telegram/cron.log

import sys
import os
from os.path import join, dirname, exists
import argparse
import logging
from logging import critical, error, info, warning, debug
from dotenv import load_dotenv
import time
import csv
import requests
from distutils.util import strtobool

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

MACHINE_USERNAME = os.environ.get("MACHINE_USERNAME")
TELEGRAM_API_KEY = os.environ.get("TELEGRAM_API_KEY")
TELEGRAM_CHAT_ID = os.environ.get("TELEGRAM_CHAT_ID")
DEBUG_TELEGRAM_CHAT_ID = os.environ.get("DEBUG_TELEGRAM_CHAT_ID")
CHIA_LOG_ANALYSIS_PATH = os.environ.get("CHIA_LOG_ANALYSIS_PATH")
CHIA_SWAR_LOGS_PATH = os.environ.get("CHIA_SWAR_LOGS_PATH")
CSV_OUTPUT_PATH = os.environ.get("CSV_OUTPUT_PATH")
SEND_STATUS_FILE = bool(strtobool(os.environ.get("SEND_STATUS_FILE")))
DEBUG_MODE = bool(strtobool(os.environ.get("DEBUG_MODE")))

outputLogFilePath =  CSV_OUTPUT_PATH + '/chia-logs-' + MACHINE_USERNAME + '.csv'
statusFilePath = CSV_OUTPUT_PATH + '/status-' + MACHINE_USERNAME + '.txt'

def parse_arguments():
    """Read arguments from a command line."""
    parser = argparse.ArgumentParser(description='Arguments get parsed via --commands')
    parser.add_argument('-v', metavar='verbosity', type=int, default=2,
        help='Verbosity of logging: 0 -critical, 1- error, 2 -warning, 3 -info, 4 -debug')

    args = parser.parse_args()
    verbose = {0: logging.CRITICAL, 1: logging.ERROR, 2: logging.WARNING, 3: logging.INFO, 4: logging.DEBUG}
    logging.basicConfig(format='%(message)s', level=verbose[args.v], stream=sys.stdout)
    
    return args

def createCsvLogFile():
  os.system('rm ' + outputLogFilePath)
  os.system('rm ' + outputLogFilePath + '.raw')
  os.system(CHIA_LOG_ANALYSIS_PATH + '/chia-log-analysis -i ' + CHIA_SWAR_LOGS_PATH + ' -o ' + outputLogFilePath + '.raw')	  
  convertCsvColumns()
  return

def convertStringTimestampToHours(timestamp):
  return time.strftime('%H:%M:%S', time.gmtime(float(timestamp))) if timestamp != '' and timestamp is not None else ''	

def convertCsvColumns():
  with open(outputLogFilePath + '.raw', "r") as source:
    csvDictReader = csv.DictReader(source)
    reader = sorted(csvDictReader, key=lambda d: d['Start date'], reverse=True)

    with open(outputLogFilePath, "w") as result:
      writer = csv.writer(result)
      writer.writerow(('Start date', 'Phase 1 duration (h)', 'Phase 2 duration (h)', 'Phase 3 duration (h)', 'Phase 4 duration (h)', 'Total time (h)', 'Copy time (h)', 'Temp dir 1', 'Plot filename'))
      
      for r in reader:			
        phase1duration = convertStringTimestampToHours(r['Phase 1 duration'])									
        phase2duration = convertStringTimestampToHours(r['Phase 2 duration'])									
        phase3duration = convertStringTimestampToHours(r['Phase 3 duration'])									
        phase4duration = convertStringTimestampToHours(r['Phase 4 duration'])									
        copyTime = convertStringTimestampToHours(r['Copy time'])									
        writer.writerow((r['Start date'], phase1duration, phase2duration, phase3duration, phase4duration, r['Total time'], copyTime, r['Temp dir 1'], r['Plot filename']))
  return

def createStatusFile():
  os.system('rm ' + statusFilePath)
  os.system('sh swar-get-status.sh -u ' + MACHINE_USERNAME)
  return

def sendFileToTelegram(filePath, chatId):
  if(exists(filePath) == False):
    error(filePath, ' does not exist')
    return

  url = f"https://api.telegram.org/bot{TELEGRAM_API_KEY}/sendDocument" 
  file = {'document': open(filePath ,'rb')} 
  queryParams = {'chat_id' : chatId }
  response = requests.post(url, data=queryParams, files=file)
  
  info(response.text)
  return

def main(args): 
    chatId = DEBUG_TELEGRAM_CHAT_ID if DEBUG_MODE == True else TELEGRAM_CHAT_ID
    if SEND_STATUS_FILE == True:
      createStatusFile()
      sendFileToTelegram(statusFilePath, chatId)

    createCsvLogFile()
    sendFileToTelegram(outputLogFilePath, chatId)
    return

if __name__ == '__main__':
    args = parse_arguments()
    main(args)