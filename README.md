## install
```
pip3 install python-dotenv
pip3 install requests
```

## start

### edit .env

```
cp .env.example .env 
nano .env
```

### run
run one time 

```
python3 cron-logs-to-telegram.py -v=4
```

### add to crotab config

`crontab -e`
add the line to run every day at 00:00, 08:00, 16:00

```
0 0,8,16 * * * python3 /home/user1/chia/chia-to-telegram/cron-logs-to-telegram.py > /home/user1/chia/chia-to-telegram/cron.log
```